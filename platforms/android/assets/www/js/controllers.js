angular.module('starter.controllers', ['ngStorage'])

    .controller('AppCtrl', function ($scope, $rootScope, $localStorage, $state, $ionicPopup) {
        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:
        //$scope.$on('$ionicView.enter', function(e) {
        //});

        // check logging in for supplier

        $scope.isLoggedIn = function () {

            if ($localStorage.supplier_id) {

                $state.go('app.supplier_settings');

            } else {

                $state.go('app.login');

            }
        };

        // logout for supplier

        $scope.logOut = function () {

            $localStorage.supplier_id = "";
            $rootScope.supplierLoggedIn = $localStorage.supplier_id;

            $state.go('app.main')

        };

        // push notifications redirect

        $rootScope.$on('newNotification', function (event, args) {

            $state.go('app.main');

        });


        $scope.checkUserRegistrationForFavorites = function(){

            if (!$localStorage.clientid) {

                $rootScope.$broadcast('makeRegistration');

                $state.go('app.main');

            } else {

                $state.go('app.favorites')

            }


        };

    })

    // Controllers

    .controller('MainCtrl', function ($ionicLoading, ClosePopupService, isFavorite1, deleteFavorite1, makeFavorite1, isOrdered1, addToOrders1, removeFromOrders1, $scope, $rootScope, $state, $localStorage, $http, $ionicPopup, $ionicScrollDelegate, $cordovaGeolocation) {

        $scope.$on('$ionicView.enter', function () {

            $rootScope.stateCurrentName = $state.current.name;

        });

        $scope.$on('makeRegistration', function () {

            $scope.checkClientLogin();

        });

        $scope.navTitle = '<img src="img/app_logo.png" style="height: 100%;display:none;">';

        // refresh the page and get new deals if they exist

        $scope.doRefresh = function () {

            $rootScope.lat = '';
            $rootScope.lng = '';

            if (window.cordova) {

                CheckGPS.check(function win(){

                    var posOptions = {timeout: 3000, enableHighAccuracy: true};

                    $cordovaGeolocation
                        .getCurrentPosition(posOptions)
                        .then(function (position) {

                            $rootScope.lat = position.coords.latitude;
                            $rootScope.lng = position.coords.longitude;

                            $rootScope.getDealsWithLocation($rootScope.lat, $rootScope.lng);


                        }, function (err) {

                            $rootScope.getDealsWithoutLocation();

                        })
                        .finally(function () {

                            $scope.$broadcast('scroll.refreshComplete');

                        });

                }, function fail(){

                    $rootScope.getDealsWithoutLocation();

                    $scope.$broadcast('scroll.refreshComplete');

                });

            }

            // var posOptions = {timeout: 5000, enableHighAccuracy: true};
            //
            // $cordovaGeolocation
            //     .getCurrentPosition(posOptions)
            //     .then(function (position) {
            //
            //         $rootScope.lat = position.coords.latitude;
            //         $rootScope.lng = position.coords.longitude;
            //
            //         $rootScope.getDealsWithLocation($rootScope.lat, $rootScope.lng);
            //         $scope.$broadcast('scroll.refreshComplete');
            //
            //     }, function(err) {
            //
            //         $rootScope.getDealsWithoutLocation();
            //         console.log('err1', err);
            //         $scope.$broadcast('scroll.refreshComplete');
            //
            //     });
        };

        // toggle Favorite icon MainCtrl

        $scope.isFavorite = function (x) {

            return isFavorite1.isFavorite1(x);

        };

        // add to Favorites MainCtrl

        $scope.makeFavoriteBtn = function (x) {

            if ($localStorage.clientid) {

                return makeFavorite1.makeFavorite1($scope, x);

            } else {

                $scope.checkClientLogin();

            }

        };

        // remove from Favorites MainCtrl

        $scope.deleteFavoriteBtn = function (x) {

            if ($localStorage.clientid) {

                return deleteFavorite1.deleteFavorite1($scope, x);

            } else {

                $scope.checkClientLogin();

            }

        };

        // check if client is logged in MainCtrl

        $scope.checkClientLogin = function (x) {

            $scope.fields = {
                "name": "",
                "phone": "",
                "push": $rootScope.pushId,
                "conditions": false
            };

            if (!$localStorage.clientid) {

                var myPopup = $ionicPopup.show({
                    templateUrl: 'templates/registration.html',
                    scope: $scope,
                    cssClass: 'registration'
                });

                // ClosePopupService.register(myPopup);

                $scope.hideRegistration = function () {

                    myPopup.close();

                };

                $scope.openConditions = function () {

                    myPopup.close();
                    $state.go('app.conditions');
                };

                $scope.makeRegistrationBtn = function () {

                    if (!$scope.fields.name || !$scope.fields.phone || $scope.fields.name == "" || $scope.fields.phone == "" || $scope.fields.phone.length < 10 || $scope.fields.conditions == false) {

                        // event.preventDefault();

                    } else {

                        $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

                        $http.post($rootScope.Host + '/user_registration.php', $scope.fields).then(
                            function (data) {       // on success
                                console.log(data);
                                $localStorage.clientid = data.data.response.userid;
                                myPopup.close();


                            },

                            function () {           // on error

                                $ionicPopup.alert({
                                    title: 'אין התחברות לבסיס נתונים 33',
                                    buttons: [{
                                        text: 'אשר',
                                        type: 'button-positive'
                                    }]
                                });

                            });
                    }
                };

            } else {

                return true
            }

        };

        // is ordered MainCtrl

        $scope.isOrdered = function (x) {

            return isOrdered1.isOrdered1(x);
        };

        // add to My Orders MainCtrl

        $scope.addToOrders = function (x, y) {

            if ($localStorage.clientid) {

                return addToOrders1.addToOrders1($scope, x, y);

            } else {

                $scope.checkClientLogin();

            }

        };

        // delete from my orders MainCtrl

        $scope.deleteFromMyOrders = function (x) {

            return removeFromOrders1.removeFromOrders1($scope, x);

        };

        // missed deal popup MainCtrl

        $scope.missedDealPopup = function(){

            var missedDealPopup = $ionicPopup.show({
                templateUrl: 'templates/missed_deal.html',
                scope: $scope,
                cssClass: 'missedDeal'
            });

            ClosePopupService.register(missedDealPopup);

            $scope.hideMissedDealPopup = function () {

                missedDealPopup.close();

            };

        };

        // show and hide info popup MainCtrl

        $scope.showInfo = function (x) {

            $scope.dealInfo = null;

            for (var i = 0; i < $rootScope.Deals.length; i++) {

                if ($rootScope.Deals[i].dealId == x) {

                    $scope.dealInfo = $rootScope.Deals[i];
                    $scope.dealInfo.hoursopenArray = [];
                    $scope.dealInfo.hoursopenSplit = $scope.dealInfo.hoursopen.split('---');
                     $scope.dealInfo.hoursopenArray[0] = $scope.dealInfo.hoursopen;
                    $scope.dealInfo.hoursopenArray[1] = '';
                    $scope.dealInfo.hoursopenArray[2] = '';
                    if($scope.dealInfo.hoursopenSplit.length>0){
                        $scope.dealInfo.hoursopenArray[0] = $scope.dealInfo.hoursopenSplit[0];
                        if($scope.dealInfo.hoursopenSplit.length>1){
                            $scope.dealInfo.hoursopenArray[1] = $scope.dealInfo.hoursopenSplit[1];
                            if($scope.dealInfo.hoursopenSplit.length>2){
                                $scope.dealInfo.hoursopenArray[2] = $scope.dealInfo.hoursopenSplit[2];
                            }  
                        }  
                    }  
                    console.log($scope.dealInfo.hoursopenSplit);
                    console.log( $scope.dealInfo.hoursopenArray);
                    console.log($scope.dealInfo);
                }

            }

            var myPopup = $ionicPopup.show({
                templateUrl: 'templates/info.html',
                scope: $scope,
                cssClass: 'infoMainDiv'
            });

            ClosePopupService.register(myPopup);

            $scope.hideInfo = function () {

                myPopup.close();

            };


            // toggle Favorite icon Info

            $scope.isFavorite = function (x) {

                return isFavorite1.isFavorite1(x);

            };

            // add to Favorites Info

            $scope.makeFavoriteBtn = function (x) {

                if ($localStorage.clientid) {

                    return makeFavorite1.makeFavorite1($scope, x);

                } else {

                    $scope.checkClientLogin();

                }

            };

            // remove from Favorites Info

            $scope.deleteFavoriteBtn = function (x) {

                if ($localStorage.clientid) {

                    return deleteFavorite1.deleteFavorite1($scope, x);

                } else {

                    $scope.checkClientLogin();

                }

            };

        };

        // scroll to top

        $scope.scrollTop = function () {

            $ionicScrollDelegate.scrollTop('shouldAnimate');

        };

        // share with WhatsApp

        $scope.shareWithWhatsApp = function (x) {

            // window.plugins.socialsharing.canShareVia('whatsapp', 'msg', null, null, null, function (e) {

                    window.plugins.socialsharing.shareViaWhatsApp('ל' + x + ' יש דיל מקסים היום!', null /* img */, 'http://sppontani.co.il/', function () {

                            console.log('share ok')

                        },
                        function (errormsg) {

                            // alert(errormsg)

                        })
                // },
                //
                // function (e) {
                //
                //     alert('No WhatsApp installed!')
                //
                // })

        };

    })

    .controller('FavoritesCtrl', function ($state, ClosePopupService, isFavorite1, makeFavorite1, deleteFavorite1, isOrdered1, addToOrders1, removeFromOrders1, $scope, $rootScope, $localStorage, $ionicPopup, $http, $ionicModal) {

       // push redirect for Notifications for today

        $scope.$on('$ionicView.enter', function () {

            $rootScope.stateCurrentName = $state.current.name;

            $rootScope.enterScreen = false;

            if ($rootScope.pushRedirect) {

                $scope.getSupplierDeals($rootScope.pushRedirect);

                $rootScope.pushRedirect = '';

            }

            // possibility to delete favorites

            $scope.trashcan = false;

        });

        // some necessary variables for view

        $scope.angular = angular;

        $scope.navTitle = '<img src="img/app_logo.png" style="height: 100%;display:none;">';

        // switching between tabs

        $scope.selection = 'new';

        // show Favorites Popup with info

        $scope.showFavoritesInfo = function () {

            var myPopup = $ionicPopup.show({
                templateUrl: 'templates/favorites_popup.html',
                scope: $scope,
                cssClass: 'favoritesPopup'
            });

            ClosePopupService.register(myPopup);

            $scope.hideFavoritesInfo = function () {

                myPopup.close();

            };
        };

        // toggle Favorite icon FavoritesCtrl

        $scope.isFavorite = function (x) {

            return isFavorite1.isFavorite1(x);

        };

        // add to Favorites FavoritesCtrl

        $scope.makeFavoriteBtn = function (x) {

            return makeFavorite1.makeFavorite1($scope, x);

        };

        // remove from Favorites FavoritesCtrl

        $scope.deleteFavoritePopupIndex = 0;

        $scope.deleteFavoriteBtn = function (x) {

            $scope.deleteFavoritePopupIndex = x;

            var deleteFavoritePopup = $ionicPopup.show({
                templateUrl: 'templates/delete_favorite.html',
                scope: $scope,
                cssClass: 'deleteOrder'
            });

            ClosePopupService.register(deleteFavoritePopup);

            $scope.hideDeleteFavoritePopup = function () {

                deleteFavoritePopup.close();

            };

        };

        $scope.deleteFavoriteAfterPopup = function (){

            $scope.hideDeleteFavoritePopup();

            return deleteFavorite1.deleteFavorite1($scope, $scope.deleteFavoritePopupIndex);


        };

        // add to My Orders FavoritesCtrl

        $scope.addToOrders = function (x, y) {

            return addToOrders1.addToOrders1($scope, x, y);

        };

        // delete from my orders FavoritesCtrl

        $scope.deleteFromMyOrders = function (x) {

            return removeFromOrders1.removeFromOrders1($scope, x);

        };

        // is ordered FavoritesCtrl

        $scope.isOrdered = function (x) {

            return isOrdered1.isOrdered1(x);
        };


        // icon with get all deals is shown or not

        $scope.showGetAllDealsIcon = function (x) {

            for (var i = 0; i < $rootScope.Deals.length; i++) {

                if ($rootScope.Deals[i].supplierId == x) {

                    return true;

                }

            }

        };

        // missed deal popup FavoritesCtrl

        $scope.missedDealPopup = function(){

            var missedDealPopup = $ionicPopup.show({
                templateUrl: 'templates/missed_deal.html',
                scope: $scope,
                cssClass: 'missedDeal'
            });

            ClosePopupService.register(missedDealPopup);

            $scope.hideMissedDealPopup = function () {

                missedDealPopup.close();

            };

        };

        // modal with all supplier deals

        $scope.supplierNewDeals = [];
        $scope.supplierOldDeals = [];

        $scope.getSupplierDeals = function (x) {

            var send_data = {
                'id': x,
                'lat' : $rootScope.lat,
                'lng' : $rootScope.lng
            };

            $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

            $http.post($rootScope.Host + '/getDealsBySupplierForFavorites.php', send_data).then(

                function (data) {       // on success

                    console.log('getSupplierDeals', data.data[0]);

                    for (var i = 0; i < data.data[0].new_deals.length; i++){

                        var deal = {
                            supplierId: data.data[0].index,
                            dealId: data.data[0].new_deals[i].index,
                            address: data.data[0].address,
                            name: data.data[0].name,
                            logo: data.data[0].image,
                            image: data.data[0].new_deals[i].image2,
                            starttime: data.data[0].new_deals[i].startime,
                            endtime: data.data[0].new_deals[i].endtime,
                            deal_date: data.data[0].new_deals[i].deal_date,
                            discount_percent: data.data[0].new_deals[i].discount_precent,
                            email: data.data[0].email,
                            phone: data.data[0].phone,
                            hoursopen: data.data[0].hoursopen,
                            location_lat: data.data[0].location_lat,
                            location_lng: data.data[0].location_lng,
                            dist: data.data[0].new_deals[i].dist,
                            quantity_ordered: Number(data.data[0].new_deals[i].quantity_ordered) + data.data[0].new_deals[i].quantity_ordered_plus,
                            kosher: data.data[0].kosher,
                            msg: 'Hello, ' + data.data[0].name + ' has great deal today!',
                            active: false
                        };
                        

                        if (deal.image == ""){
                            deal.image =  data.data[0].image2;
                        }

                        if (deal.endtime > '05:00' && $rootScope.timeNow > '05:00') {

                            if ($rootScope.timeNow <= deal.endtime){

                                deal.active = true;

                            }

                        } else if (deal.endtime <= '05:00' && $rootScope.timeNow > '05:00') {

                            deal.active = true;

                        } else if (deal.endtime <= '05:00' && $rootScope.timeNow <= '05:00') {

                            if ($rootScope.timeNow <= deal.endtime){

                                deal.active = true;

                            }

                        }

                        $scope.supplierNewDeals.push(deal);

                    }

                    for (var j = 0; j < data.data[0].old_deals.length; j++){

                        var deal = {
                            supplierId: data.data[0].index,
                            dealId: data.data[0].old_deals[j].index,
                            address: data.data[0].address,
                            name: data.data[0].name,
                            logo: data.data[0].image,
                            image: data.data[0].old_deals[j].image2,
                            starttime: data.data[0].old_deals[j].startime,
                            endtime: data.data[0].old_deals[j].endtime,
                            deal_date: data.data[0].old_deals[j].deal_date,
                            discount_percent: data.data[0].old_deals[j].discount_precent,
                            email: data.data[0].email,
                            phone: data.data[0].phone,
                            hoursopen: data.data[0].hoursopen,
                            location_lat: data.data[0].location_lat,
                            location_lng: data.data[0].location_lng,
                            dist: data.data[0].old_deals[j].dist,
                            quantity_ordered: Number(data.data[0].old_deals[j].quantity_ordered) + data.data[0].old_deals[j].quantity_ordered_plus,
                            kosher: data.data[0].kosher,
                            msg: 'Hello, ' + data.data[0].name + 'has great deal today!'
                        };

                        if (deal.image == ""){
                            deal.image =  data.data[0].image2;
                        }

                        $scope.supplierOldDeals.push(deal);

                    }

                    console.log($scope.supplierNewDeals);
                    console.log($scope.supplierOldDeals);

                },

                function () {           // on error

                    $ionicPopup.alert({
                        title: 'אין התחברות לבסיס נתונים 213',
                        buttons: [{
                            text: 'אשר',
                            type: 'button-positive'
                        }]
                    });

                });

                $ionicModal.fromTemplateUrl('templates/get_supplier_deals.html', {

                    scope: $scope,
                    animation: 'slide-in-up'

                }).then(function (modal) {

                    $scope.modal = modal;
                    $scope.modal.show();


            });

        };

        // close modal

        $scope.hideSupplierDeals = function () {

            $scope.modal.hide();
            $scope.supplierNewDeals = [];
            $scope.supplierOldDeals = [];

        };

        // show and hide info popup FavoritesCtrl

        $scope.showSupplierInfo = function (x) {

            $scope.dealInfo = null;

            if($state.current.name == 'app.favorites') {

                for (var i = 0; i < $rootScope.Deals.length; i++) {

                    if ($rootScope.Deals[i].dealId == x) {

                        $scope.dealInfo = $rootScope.Deals[i];

                    }

                }

            } else if ($state.current.name == 'app.add_favorites'){

                for (var j = 0; j < $scope.searchSupplierData.length; j++) {

                    if ($scope.searchSupplierData[j].supplierId == x) {

                        $scope.dealInfo = $scope.searchSupplierData[j];

                    }

                }

            }


            var myPopup = $ionicPopup.show({
                templateUrl: 'templates/info2.html',
                scope: $scope,
                cssClass: 'infoMainDiv'
            });

            ClosePopupService.register(myPopup);

            $scope.hideInfo = function () {

                myPopup.close();

            };

            // toggle Favorite icon Info

            $scope.isFavorite = function (x) {

                return isFavorite1.isFavorite1(x);

            };

            // add to Favorites Info

            $scope.makeFavoriteBtn = function (x) {

                return makeFavorite1.makeFavorite1($scope, x);

            };

            // remove from Favorites Info

            $scope.deleteFavoriteBtn = function (x) {

                return deleteFavorite1.deleteFavorite1($scope, x);

            };

            // share with WhatsApp Favorites Info

            $scope.shareWithWhatsApp = function (x) {

                // window.plugins.socialsharing.canShareVia('whatsapp', 'msg', null, null, null, function (e) {

                        window.plugins.socialsharing.shareViaWhatsApp('ל' + x + ' יש דיל מקסים היום!', null /* img */, 'http://sppontani.co.il/', function () {

                                console.log('share ok')

                            },
                            function (errormsg) {

                                // alert(errormsg)

                            })
                    // },
                    //
                    // function (e) {
                    //
                    //     alert('No WhatsApp installed!')
                    //
                    // })

            }

        };


        // suppliers search for FavoritesCtrl

        $scope.favorites_search = {"data" : ""};
        $scope.searchSupplierData = [];
        $scope.noSuppliers = false;

        // if keyboard event is used

        $scope.myFunct = function(keyEvent) {

          //  if (keyEvent.which === 13) {

                $scope.searchSuppliers($scope.favorites_search.data);

          //  }
        };

        // if button is pressed

        $scope.searchSuppliers = function(x){

            var search = {

                'search' : x

            };

      /*      if (x == '') {

                $scope.noSuppliers = false;

                $ionicPopup.alert({
                    title: 'נא להזין שם מסעדה',
                    buttons: [{
                        text: 'אשר',
                        type: 'button-positive'
                    }]
                });

            } else {*/

                $scope.noSuppliers = false;
                $scope.searchSupplierData = [];

                $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

                $http.post($rootScope.Host + '/search_supplier.php', search).then(

                    function (data) {       // on success

                        console.log(data.data[0]);

                        if (data.data[0].length == 0){

                            $scope.favorites_search = {"data" : ""};
                            $scope.searchSupplierData = [];
                            $scope.noSuppliers = true;

                        } else {

                            for (var i = 0; i < data.data[0].length; i++){

                                var supplier = {

                                    supplierId: data.data[0][i].index,
                                    name : data.data[0][i].name,
                                    address : data.data[0][i].address,
                                    logo: data.data[0][i].image,
                                    deals_number: data.data[0][i].deals_number,
                                    image: data.data[0][i].image2,
                                    hoursopen: data.data[0][i].hoursopen,
                                    phone: data.data[0][i].phone

                                };

                                $scope.searchSupplierData.push(supplier);

                            }
                            console.log("searchSupplierData", $scope.searchSupplierData);

                          //  $scope.favorites_search = {"data" : ""};

                        }

                    },

                    function () {           // on error

                        $ionicPopup.alert({
                            title: 'אין התחברות לבסיס נתונים 214',
                            buttons: [{
                                text: 'אשר',
                                type: 'button-positive'
                            }]
                        });

                    });

           // }

        };

        $scope.$on('$ionicView.leave', function () {

            $scope.searchSupplierData = [];

            $scope.favorites_search = {"data" : ""};

            $scope.noSuppliers = false;

            // $scope.trashcan = false;
        });

    })

    .controller('MyOrdersCtrl', function ($state, ClosePopupService, isFavorite1, makeFavorite1, deleteFavorite1, removeFromOrders1, isOrdered1, $scope, $rootScope, $localStorage, $http, $ionicPopup) {

        $scope.$on('$ionicView.enter', function () {

            $rootScope.stateCurrentName = $state.current.name;

            $rootScope.enterScreen = false;

        });

        $scope.navTitle = '<img src="img/app_logo.png" style="height: 100%;display:none;">';

        // delete from my orders MyOrdersCtrl

        $scope.deleteFromMyOrders = function (x) {

            return removeFromOrders1.removeFromOrders1($scope, x);
        };

        // is ordered MyOrdersCtrl

        $scope.isOrdered = function (x) {

            return isOrdered1.isOrdered1(x);
        };

        $scope.showSupplierInfo = function (x) {

            $scope.dealInfo = null;

            for (var i = 0; i < $rootScope.myOrders.length; i++) {

                if ($rootScope.myOrders[i].dealId == x) {

                    $scope.dealInfo = $rootScope.myOrders[i];
                    console.log($scope.dealInfo);
                }

            }

            var myPopup = $ionicPopup.show({
                templateUrl: 'templates/info2.html',
                scope: $scope,
                cssClass: 'infoMainDiv'
            });

            ClosePopupService.register(myPopup);

            $scope.hideInfo = function () {

                myPopup.close();

            };


            // toggle Favorite icon Info

            $scope.isFavorite = function (x) {

                return isFavorite1.isFavorite1(x);

            };

            // add to Favorites Info

            $scope.makeFavoriteBtn = function (x) {

                return makeFavorite1.makeFavorite1($scope, x);

            };

            // remove from Favorites Info

            $scope.deleteFavoriteBtn = function (x) {

                return deleteFavorite1.deleteFavorite1($scope, x);

            };

        };

    })

    .controller('LoginCtrl', function ($scope, $http, $ionicPopup, $rootScope, $localStorage, $state, $ionicModal) {

        $scope.$on('$ionicView.enter', function () {

            $rootScope.stateCurrentName = $state.current.name;

            $rootScope.enterScreen = false;

        });

        $scope.navTitle = '<img src="img/app_logo.png" style="height: 100%;display:none;">';

        if ($localStorage.supplier_id) {
            $state.go('app.supplier_settings');
        }

        $scope.login =
        {
            "username": "",
            "password": ""
        };

        $scope.loginBtn = function () {

            $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

            if ($scope.login.username == "") {

                $ionicPopup.alert({
                    title: 'יש להזין שם משתמש',
                    buttons: [{
                        text: 'אשר',
                        type: 'button-positive'
                    }]
                });

            }

            else if ($scope.login.password == "") {

                $ionicPopup.alert({
                    title: 'יש להזין סיסמא',
                    buttons: [{
                        text: 'אשר',
                        type: 'button-positive'
                    }]
                });

            }

            else {

                var login_data =
                {
                    "username": $scope.login.username,
                    "password": $scope.login.password
                };

                $http.post($rootScope.Host + '/supplier_login.php', login_data).then(
                    function (data) {           // on success

                        $localStorage.supplier_id = data.data[0].index;
                        $rootScope.supplierLoggedIn = $localStorage.supplier_id;

                        $scope.login =
                        {
                            "username": "",
                            "password": ""
                        };

                        $state.go('app.supplier_settings');

                    },
                    function () {           // on error

                        $ionicPopup.alert({
                            title: 'שם משתמש או סיסמה שגוים נא לתקן',
                            buttons: [{
                                text: 'אשר',
                                type: 'button-positive'
                            }]
                        });

                    });


            }
        };

        // forgot password

        $scope.forgot =
        {
            "phone": ""
        };

        $scope.forgotPasswordBtn = function () {

            $ionicModal.fromTemplateUrl('templates/forgot_pass.html', {

                scope: $scope

            }).then(function (ForgotPassModal) {

                $scope.ForgotPassModal = ForgotPassModal;
                $scope.ForgotPassModal.show();

            });
        };

        $scope.sendPasswordBtn = function () {

            if ($scope.forgot.phone == "" || !$scope.forgot.phone || $scope.forgot.phone.length < 10) {
                $ionicPopup.alert({
                    title: 'יש להזין מספר טלפון תקין',
                    buttons: [{
                        text: 'אשר',
                        type: 'button-positive'
                    }]
                });
            }
            else {

                var send_data =
                {
                    "name" : "No name",
                    "phone": $scope.forgot.phone,
                    "text" : "שכחתי סיסמה"

                };

                $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

                $http.post($rootScope.Host + '/sendMessage.php', send_data).then(
                    function (data) {       // on success

                        if (data.data.response.status == 0){

                            $ionicPopup.alert({
                                title: 'יתקשרו אליך בזמן הקרוב!',
                                buttons: [{
                                    text: 'אשר',
                                    type: 'button-positive'
                                }]
                            });

                            $scope.forgot.phone = '';

                        }

                    },

                    function () {         // on error

                        $ionicPopup.alert({
                            title: 'אין התחברות לבסיס נתונים199',
                            buttons: [{
                                text: 'אשר',
                                type: 'button-positive'
                            }]
                        });

                    }
                );

                $scope.ForgotPassModal.hide();
            }
        };

        $scope.closeModal = function () {

            $scope.ForgotPassModal.hide();

        };

        // contact form

        $scope.showContactForm = function(){

            $ionicModal.fromTemplateUrl('templates/contact_form.html', {

                scope: $scope

            }).then(function (ContactFormModal) {

                $scope.ContactFormModal = ContactFormModal;
                $scope.ContactFormModal.show();

            });
        };

        $scope.closeContactModal = function () {

            $scope.ContactFormModal.hide();

        };

        $scope.contact = {
            "name": "",
            "phone": "",
            "text": ""
        };

        $scope.sendMessage = function(x, y, z){

            if (x == "" || y == "" || z == "" || !x || !y || !z){

                $ionicPopup.alert({
                    title: 'נא למלא את כל השדות',
                    buttons: [{
                        text: 'אשר',
                        type: 'button-positive'
                    }]
                });

            } else {

                $scope.ContactFormModal.hide();

                $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

                $http.post($rootScope.Host + '/sendMessage.php', $scope.contact).then(
                    function (data) {       // on success

                        $ionicPopup.alert({
                            title: 'תודה שפניתם אלינו!',
                            buttons: [{
                                text: 'אשר',
                                type: 'button-positive'
                            }]
                        });

                        $scope.contact = {
                            "name": "",
                            "phone": "",
                            "text": ""
                        };

                    },
                    function () {           // on error

                        $ionicPopup.alert({
                            title: 'אין התחברות לבסיס נתונים 125',
                            buttons: [{
                                text: 'אשר',
                                type: 'button-positive'
                            }]
                        });

                    });

            }

        };

    })

    .controller('SupplierCtrl', function ($scope, $state, $localStorage, $http, $rootScope, $ionicPopup, $ionicModal, $timeout) {

        $scope.$on('$ionicView.enter', function () {

            $rootScope.stateCurrentName = $state.current.name;

            $rootScope.enterScreen = false;

            // request to database to get all info

            $scope.supplierNewData = [];
            $scope.supplierOldData = [];

            $scope.supplierDetails = {
                address: '',
                name: '',
                logo: '',
                image: '',
                email: '',
                phone: '',
                hoursopen: '',
                location_lat: '',
                location_lng: '',
                dist: '',
                kosher: ''
            };


            var loginid =
            {
                "id": $localStorage.supplier_id
            };

            $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

            $http.post($rootScope.Host + '/getDealsBySupplier.php', loginid).then(
                function (data) {       // on success

                    $scope.supplierDetails = {
                        address: data.data[0].address,
                        name: data.data[0].name,
                        logo: data.data[0].image,
                        image: data.data[0].image2,
                        email: data.data[0].email,
                        phone: data.data[0].phone,
                        hoursopen: data.data[0].hoursopen,
                        location_lat: data.data[0].location_lat,
                        location_lng: data.data[0].location_lng,
                        dist: data.data[0].dist,
                        kosher: data.data[0].kosher
                    };

                    for (var j = 0; j < data.data[0].new_deals.length; j++) {

                        var deal = {
                            supplierId: data.data[0].index,
                            dealId: data.data[0].new_deals[j].index,
                            starttime: data.data[0].new_deals[j].startime,
                            endtime: data.data[0].new_deals[j].endtime,
                            deal_date: data.data[0].new_deals[j].deal_date,
                            discount_percent: data.data[0].new_deals[j].discount_precent,
                            quantity_ordered: Number(data.data[0].new_deals[j].quantity_ordered) + data.data[0].new_deals[j].quantity_ordered_plus
                        };

                        $scope.supplierNewData.push(deal);

                    }

                    for (var j = 0; j < data.data[0].old_deals.length; j++) {

                        var deal = {
                            supplierId: data.data[0].index,
                            dealId: data.data[0].old_deals[j].index,
                            starttime: data.data[0].old_deals[j].startime,
                            endtime: data.data[0].old_deals[j].endtime,
                            deal_date: data.data[0].old_deals[j].deal_date,
                            discount_percent: data.data[0].old_deals[j].discount_precent,
                            quantity_ordered: Number(data.data[0].old_deals[j].quantity_ordered) + data.data[0].old_deals[j].quantity_ordered_plus
                        };

                        $scope.supplierOldData.push(deal);

                    }
                    console.log('SupplierData', $scope.supplierNewData, $scope.supplierOldData);

                },
                function () {           // on error

                    $ionicPopup.alert({
                        title: 'אין התחברות לבסיס נתונים100',
                        buttons: [{
                            text: 'אשר',
                            type: 'button-positive'
                        }]
                    });

                });

        });


        $scope.navTitle = '<img src="img/app_logo.png" style="height: 100%;display:none;">';

        // switching between tabs

        $scope.selection = 'new';

        // navigate back to login if not logged in

        if (!$localStorage.supplier_id) {

            $state.go('app.login');

        }

        // new deal modal

        $scope.addNewDeal = function () {

            $ionicModal.fromTemplateUrl('templates/add_new_deal.html', {

                scope: $scope,
                animation: 'slide-in-up'

            }).then(function (modal) {

                $scope.modal = modal;
                $scope.modal.show();

            });

        };

        // close new deal modal

        $scope.hideNewDeal = function () {

            $scope.modal.hide();

            $scope.fields = {
                "date": "",
                "starttime": "",
                "endtime": "",
                "discount_percent": ""
            };

        };

        // datepicker

        $scope.fields = {
            "date": "",
            "starttime": "",
            "endtime": "",
            "discount_percent": ""
        };

        $scope.newDate = "";

        $scope.pickDate = function () {

            var options = {
                date: new Date(),
                mode: 'date',
                allowOldDates: false
            };

            function onSuccess(data) {

                $scope.newDate = data.getFullYear() + '-' + (data.getMonth() < 10 ? '0' : '') + (data.getMonth() + 1) + '-' + (data.getDate() < 10 ? '0' : '') + data.getDate();

                $scope.updateDate();

            }

            function onError(error) { // Android only

                $ionicPopup.alert({
                    title: 'נא למלא את כל השדות',
                    buttons: [{
                        text: 'אשר',
                        type: 'button-positive'
                    }]
                });
            }

            datePicker.show(options, onSuccess, onError);

        };

        $scope.updateDate = function () {
            $timeout(function () {

                $scope.fields.date = $scope.newDate;
            }, 100);
        };

        // pick start time

        $scope.newStartTime = '';

        $scope.pickStartTime = function () {

            var options = {
                date: new Date(),
                mode: 'time',
                minuteInterval: 10,
                is24Hour: true
            };

            function onSuccess(data) {

                $scope.newStartTime = (data.getHours() < 10 ? '0' : '') + data.getHours() + ':' + (data.getMinutes() < 10 ? '0' : '') + data.getMinutes();

                $scope.updateStartTime();
            }

            function onError(error) { // Android only

                $ionicPopup.alert({
                    title: 'נא למלא את כל השדות',
                    buttons: [{
                        text: 'אשר',
                        type: 'button-positive'
                    }]
                });
            }

            datePicker.show(options, onSuccess, onError);
        };

        $scope.updateStartTime = function () {
            $timeout(function () {

                $scope.fields.starttime = $scope.newStartTime;

            }, 100);
        };

        // pick end time

        $scope.newEndTime = '';

        $scope.pickEndTime = function () {

            var options = {
                date: new Date(),
                mode: 'time',
                minuteInterval: 10,
                is24Hour: true
            };

            function onSuccess(data) {

                $scope.newEndTime = (data.getHours() < 10 ? '0' : '') + data.getHours() + ':' + (data.getMinutes() < 10 ? '0' : '') + data.getMinutes();

                $scope.updateEndTime();
            }

            function onError(error) { // Android only

                $ionicPopup.alert({
                    title: 'נא למלא את כל השדות',
                    buttons: [{
                        text: 'אשר',
                        type: 'button-positive'
                    }]
                });
            }

            datePicker.show(options, onSuccess, onError);
        };

        $scope.updateEndTime = function () {
            $timeout(function () {

                $scope.fields.endtime = $scope.newEndTime;

            }, 100);
        };

        // request to database - submit new deal

        $scope.submitNewDeal = function (x) {

            if (x == "" || $scope.fields.date == "" || $scope.fields.starttime == "" || $scope.fields.endtime == ""
                || !x || !$scope.fields.date || !$scope.fields.starttime || !$scope.fields.endtime) {

                $ionicPopup.alert({
                    title: 'נא למלא את כל השדות',
                    buttons: [{
                        text: 'אשר',
                        type: 'button-positive'
                    }]
                });

            } else if (x < 5) {

                $ionicPopup.alert({
                    title: 'מינימום אחוזי הנחה: 5%',
                    buttons: [{
                        text: 'אשר',
                        type: 'button-positive'
                    }]
                });

            } else {

                var new_deal = {
                    "supplier_index": $localStorage.supplier_id,
                    "discount_percent": x,
                    "starttime": $scope.fields.starttime,
                    "endtime": $scope.fields.endtime,
                    "deal_date": $scope.fields.date,
                    "quantity_ordered": '0'
                };

                // var new_deal = {
                //     "supplier_index": $localStorage.supplier_id,
                //     "discount_percent": x,
                //     "starttime": '14:00',
                //     "endtime": "15:00",
                //     "deal_date": "2016-08-05",
                //     "quantity_ordered": y
                // };

                $scope.hideNewDeal();

                $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

                $http.post($rootScope.Host + '/submit_new_deal.php', new_deal).then(
                    function (data) {       // on success

                        if (data.data.response.status == 0) {

                            var added_deal = {
                                supplierId: $localStorage.supplier_id,
                                dealId: data.data.added_deal.index,
                                starttime: data.data.added_deal.startime,
                                endtime: data.data.added_deal.endtime,
                                deal_date: data.data.added_deal.deal_date,
                                discount_percent: data.data.added_deal.discount_precent,
                                quantity_ordered: '0'
                            };

                            if (added_deal.deal_date >= $rootScope.today){

                                $scope.supplierNewData.push(added_deal);

                            } else {

                                $scope.supplierOldData.push(added_deal);

                            }


                            $scope.fields = {
                                "date": "",
                                "starttime": "",
                                "endtime": "",
                                "discount_percent": ""
                            };

                        }

                    },

                    function () {           // on error

                        $ionicPopup.alert({
                            title: 'אין התחברות לבסיס נתונים, הדיל לא נוסף',
                            buttons: [{
                                text: 'אשר',
                                type: 'button-positive'
                            }]
                        });

                    });
            }
        };


        // delete deal

        $scope.deleteDeal = function (x, y) {

            var deal = {
                "dealid": x
            };

            $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

            $http.post($rootScope.Host + '/delete_deal.php', deal).then(
                function (data) {       // on success

                    console.log(data);

                    if (data.data.response[0].status == 1) {

                        if (y >= $rootScope.today) {

                            for (var i = 0; i < $scope.supplierNewData.length; i++) {

                                if ($scope.supplierNewData[i].dealId == x) {

                                    $scope.supplierNewData.splice(i, 1);

                                }

                            }

                        } else {

                            for (var j = 0; j < $scope.supplierOldData.length; j++) {

                                if ($scope.supplierOldData[j].dealId == x) {

                                    $scope.supplierOldData.splice(j, 1);

                                }

                            }

                        }

                        for (var k = 0; k < $rootScope.Deals.length; k++) {

                            if ($rootScope.Deals[k].dealId == x) {

                                $rootScope.Deals.splice(k, 1);

                            }

                        }
                    }

                },

                function () {           // on error

                    $ionicPopup.alert({
                        title: '102אין התחברות לבסיס נתונים',
                        buttons: [{
                            text: 'אשר',
                            type: 'button-positive'
                        }]
                    });

                });

        };

    });

