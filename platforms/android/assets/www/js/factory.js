angular.module('starter.factories', [])

    .factory('isFavorite1', function ($rootScope) {

        return {

            isFavorite1: function(x){

                if (Object.keys($rootScope.favoriteSuppliers).indexOf(x) >= 0) {
                    return true;
                }

            }
        };


    })

    .factory('makeFavorite1', function (ClosePopupService, $rootScope, $http, $ionicPopup, $localStorage) {

        return {

            makeFavorite1: function(scope, x){

                scope.send_data = {
                    "supplier_id": x,
                    "user_id": $localStorage.clientid
                };

                var myPopup = $ionicPopup.show({
                    templateUrl: 'templates/favorite_added.html',
                    scope: scope,
                    cssClass: 'favoriteAdded'
                });

                ClosePopupService.register(myPopup);

                scope.hideFavoriteAdded = function () {

                    myPopup.close();

                };

                $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

                $http.post($rootScope.Host + '/add_favorite.php', scope.send_data).then(
                    function (data) {       // on success

                        if (data.data.response.status == 0) {

                            $rootScope.favoriteSuppliers[x] = data.data.supplier;

                        }

                    },

                    function () {           // on error

                        $ionicPopup.alert({
                            title: 'אין התחברות לבסיס נתונים, המסעדה לא נוספה למעודפים',
                            buttons: [{
                                text: 'אשר',
                                type: 'button-positive'
                            }]
                        });

                    });

            }
        };


    })


    .factory('deleteFavorite1', function ($rootScope, $http, $ionicPopup, $localStorage) {

        return {

            deleteFavorite1: function(scope, x){

                scope.send_data = {
                    "supplier": x,
                    "user": $localStorage.clientid
                };

                delete $rootScope.favoriteSuppliers[x];

                $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

                $http.post($rootScope.Host + '/delete_favorites.php', scope.send_data).then(
                    function (data) {       // on success

                        if (data.data.response[0].status == 1) {

                            //do nothing

                        }

                    },

                    function () {           // on error

                        $rootScope.favoriteSuppliers[x] = x;

                        $ionicPopup.alert({
                            title: 'אין התחברות לבסיס נתונים,המסעדה עדיין במועדפים',
                            buttons: [{
                                text: 'אשר',
                                type: 'button-positive'
                            }]
                        });

                    });

            }
        };

    })

    .factory('isOrdered1', function ($rootScope) {

        return {

            isOrdered1: function(x){

                for (var i = 0; i < $rootScope.myOrders.length; i++) {

                    if ($rootScope.myOrders[i].dealId == x) {

                        return true;

                    }
                }

            }
        };

    })

    .factory('addToOrders1', function(ClosePopupService, $rootScope, $http, $ionicPopup, $localStorage) {

        return {

            addToOrders1: function(scope, x, y) {

                scope.send_data = {
                    "user": $localStorage.clientid,
                    "supplierindex": x,
                    "dealid": y
                };

                $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

                $http.post($rootScope.Host + '/add_order.php', scope.send_data).then(
                    function (data) {       // on success

                        if (data.data.response.status == 0) {

                            for (var i = 0; i < $rootScope.Deals.length; i++) {

                                if ($rootScope.Deals[i].dealId == y) {

                                    $rootScope.myOrders.push($rootScope.Deals[i]);
                                    $rootScope.Deals[i].quantity_ordered += 1;

                                    var orderAddedPopup = $ionicPopup.show({
                                        templateUrl: 'templates/order_added.html',
                                        scope: scope,
                                        cssClass: 'orderAdded'
                                    });

                                    ClosePopupService.register(orderAddedPopup);

                                    scope.hideOrderAdded = function () {

                                        orderAddedPopup.close();

                                    };

                                }

                            }

                        }

                    },

                    function () {           // on error

                        $ionicPopup.alert({
                            title: 'אין התחברות לבסיס נתונים44',
                            buttons: [{
                                text: 'אשר',
                                type: 'button-positive'
                            }]
                        });

                    });

            }
        }

    })

    .factory('removeFromOrders1', function (ClosePopupService, $rootScope, $http, $ionicPopup, $localStorage) {

        return {

            removeFromOrders1: function(scope, x){

                var orderToDelete = x;

                var deleteOrderPopup = $ionicPopup.show({
                    templateUrl: 'templates/delete_order.html',
                    scope: scope,
                    cssClass: 'deleteOrder'
                });

                ClosePopupService.register(deleteOrderPopup);

                scope.hideDeleteOrderPopup = function () {

                    deleteOrderPopup.close();

                };

                scope.deleteOrder = function () {

                    var send_data = {
                        "dealid": orderToDelete,
                        "userid": $localStorage.clientid
                    };

                    $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

                    $http.post($rootScope.Host + '/delete_order.php', send_data).then(
                        function (data) {       // on success

                            if (data.data.response[0].status == 1) {

                                for (var i = 0; i < $rootScope.myOrders.length; i++) {

                                    if ($rootScope.myOrders[i].dealId == x) {

                                        $rootScope.myOrders.splice(i, 1);

                                        for (var j = 0; j < $rootScope.Deals.length; j++){

                                            if ($rootScope.Deals[j].dealId == x){
                                                $rootScope.Deals[j].quantity_ordered -= 1;
                                            }

                                        }

                                        scope.isOrdered(x);
                                        deleteOrderPopup.close();

                                    }

                                }

                            }

                        },

                        function () {           // on error

                            $ionicPopup.alert({
                                title: 'אין התחברות לבסיס נתונים 222',
                                buttons: [{
                                    text: 'אשר',
                                    type: 'button-positive'
                                }]
                            });

                        });
                }
            }
        };

    })

    .factory('ClosePopupService', function($document, $ionicPopup, $timeout){
        var lastPopup;
        return {
            register: function(popup) {
                $timeout(function(){
                    var element = $ionicPopup._popupStack.length>0 ? $ionicPopup._popupStack[0].element : null;
                    if(!element || !popup || !popup.close) return;
                    element = element && element.children ? angular.element(element.children()[0]) : null;
                    lastPopup  = popup;
                    var insideClickHandler = function(event){
                        event.stopPropagation();
                    };
                    var outsideHandler = function() {
                        popup.close();
                    };
                    element.on('click', insideClickHandler);
                    $document.on('click', outsideHandler);
                    popup.then(function(){
                        lastPopup = null;
                        element.off('click', insideClickHandler);
                        $document.off('click', outsideHandler);
                    });
                });
            },
            closeActivePopup: function(){
                if(lastPopup) {
                    $timeout(lastPopup.close);
                    return lastPopup;
                }
            }
        };
    });
