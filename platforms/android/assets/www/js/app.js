// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.factories', 'ngStorage', 'ngCordova', 'google.places', 'ionic.ion.imageCacheFactory'])

    .run(function ($ionicPlatform, $rootScope, $ImageCacheFactory, $ionicLoading, $http, $ionicPopup, $localStorage, $state, $cordovaGeolocation) {

       /* $ionicLoading.show({
            //template: '...מחפש דילים קרובים אלייך<ion-spinner icon="spiral"></ion-spinner>'
        }
        );*/

        setTimeout(function(){ 
              iconSpinner();
         }, 1000);
         iconSpinnerLoad=true;

         function iconSpinner(){
             if(iconSpinnerLoad){
                 if(document.getElementsByClassName('infinityIconLoading')[0].style.transform!=="rotate(360deg)"){
                    document.getElementsByClassName('infinityIconLoading')[0].style="transform:rotate(360deg)";
                 }
                else{
                    document.getElementsByClassName('infinityIconLoading')[0].style="transform:rotate(0deg)";
                }
                setTimeout(function(){ 
                    iconSpinner();
                }, 1500);
             }
            
         }

        $ionicPlatform.ready(function () {

            //push notifications

            var notificationOpenedCallback = function (jsonData) {
                //alert (JSON.stringify(jsonData));
                //alert (jsonData.additionalData.type);

                if (jsonData.additionalData.type == "newdeal") {

                    $rootScope.$broadcast('newNotification');

                }

                if (jsonData.additionalData.type == "newDealForToday") {

                    $rootScope.pushRedirect = jsonData.additionalData.supplierId;

                    if ($rootScope.pushRedirect) {

                        $rootScope.$broadcast('newDealForToday');

                        $ionicLoading.show({
                            template: '...טוען<ion-spinner icon="spiral"></ion-spinner>'
                        });

                        if (window.cordova) {

                            cordova.plugins.diagnostic.isLocationAvailable(function (available) {

                                var posOptions = {timeout: 5000, enableHighAccuracy: true};

                                $cordovaGeolocation
                                    .getCurrentPosition(posOptions)
                                    .then(function (position) {

                                        $rootScope.lat = position.coords.latitude;
                                        $rootScope.lng = position.coords.longitude;

                                        $rootScope.getDealsWithLocation($rootScope.lat, $rootScope.lng);


                                    }, function (err) {

                                        $rootScope.getDealsWithoutLocation();

                                    })
                                    .then(function () {

                                        $ionicLoading.hide();
                                        $state.go('app.favorites');

                                    });

                            }, function (error) {

                                $rootScope.getDealsWithoutLocation();

                                $ionicLoading.hide();
                                $state.go('app.favorites');

                            });
                        }

                    }
                }

                console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
            };

            window.plugins.OneSignal.init("e7dd31a1-6515-4720-8fc4-0fc36a22e3c4",
                {googleProjectNumber: "151317212448"},
                notificationOpenedCallback);

            window.plugins.OneSignal.getIds(function (ids) {

                $rootScope.pushId = ids.userId;

            });

            // Show an alert box if a notification comes in when the user is in your app.
            window.plugins.OneSignal.enableInAppAlertNotification(true);

            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });

        // Global variables

        $rootScope.Host = 'http://www.tapper.co.il/spontani/php/';
        $rootScope.Deals = [];
        $rootScope.myOrders = [];
        $rootScope.favoriteSuppliers = {};
        $rootScope.images = [];
        $rootScope.supplierLoggedIn = $localStorage.supplier_id;
        $rootScope.stateCurrentName = 'app.main';
        $rootScope.timeNow = (new Date().getHours() < 10 ? '0' : '') + new Date().getHours() + ':' + (new Date().getMinutes() < 10 ? '0' : '') + new Date().getMinutes();
        $rootScope.today = new Date().getFullYear() + '-' + (new Date().getMonth() < 10 ? '0' : '') + (new Date().getMonth() + 1) + '-' + (new Date().getDate() < 10 ? '0' : '') + new Date().getDate();
        $rootScope.enterScreen = true;

        $rootScope.lat = '';
        $rootScope.lng = '';

        // on enter at once

        if(window.cordova) {

            $ionicPlatform.ready(function () {

                CheckGPS.check(function win() {

                        var posOptions = {timeout: 3000, enableHighAccuracy: true};

                        $cordovaGeolocation
                            .getCurrentPosition(posOptions)
                            .then(function (position) {

                                $rootScope.lat = position.coords.latitude;
                                $rootScope.lng = position.coords.longitude;

                                $rootScope.getDealsWithLocation($rootScope.lat, $rootScope.lng);

                            }, function (err) {

                                $rootScope.getDealsWithoutLocation();

                            });

                    },

                    function fail() {       // if there is no GPS

                            var GPSalert = $ionicPopup.confirm({            // show alert
                                title: 'Your GPS is Disabled.',
                                template: 'Please enable location for proper work of the application'
                            });

                        GPSalert.then(function(res) {

                                if(res) {

                                    if (ionic.Platform.isIOS()){        // if the user chose to switch on GPS and it's IOS - get his deals

                                        cordova.plugins.diagnostic.switchToSettings(function(){}, function(){});

                                        document.addEventListener("resume", onResumeIOS, false);

                                        function onResumeIOS() {

                                            var posOptions = {timeout: 3000, enableHighAccuracy: true};

                                            $cordovaGeolocation
                                                .getCurrentPosition(posOptions)
                                                .then(function (position) {

                                                    $rootScope.lat = position.coords.latitude;
                                                    $rootScope.lng = position.coords.longitude;

                                                    $rootScope.getDealsWithLocation($rootScope.lat, $rootScope.lng);

                                                }, function (err) {

                                                    $rootScope.getDealsWithoutLocation();

                                                });
                                        }


                                    } else {        // if the user chose to switch on GPS and it's Android - get his deals

                                        cordova.plugins.diagnostic.switchToLocationSettings();

                                        document.addEventListener("resume", onResume, false);

                                        function onResume() {

                                            var posOptions = {timeout: 3000, enableHighAccuracy: true};

                                            $cordovaGeolocation
                                                .getCurrentPosition(posOptions)
                                                .then(function (position) {

                                                    $rootScope.lat = position.coords.latitude;
                                                    $rootScope.lng = position.coords.longitude;

                                                    $rootScope.getDealsWithLocation($rootScope.lat, $rootScope.lng);

                                                }, function (err) {

                                                    $rootScope.getDealsWithoutLocation();

                                                });
                                        }

                                    }

                                } else {        // if the user doesn't want to switch on GPS

                                    $rootScope.getDealsWithoutLocation();

                                }
                            });


                        // cordova.dialogGPS("Your GPS is Disabled.",
                        //     'Please enable location for proper work of the application',
                        //
                        //     function (buttonIndex) {
                        //
                        //         switch (buttonIndex) {
                        //             case 0:     // no
                        //
                        //                 $rootScope.getDealsWithoutLocation();
                        //                 break;
                        //
                        //             case 1:     // neutral
                        //
                        //                 $rootScope.getDealsWithoutLocation();
                        //                 break;
                        //
                        //             case 2:     // yes, go to settings
                        //
                        //                 document.addEventListener("resume", onResume, false);
                        //
                        //             function onResume() {
                        //
                        //                 var posOptions = {timeout: 3000, enableHighAccuracy: true};
                        //
                        //                 $cordovaGeolocation
                        //                     .getCurrentPosition(posOptions)
                        //                     .then(function (position) {
                        //
                        //                         $rootScope.lat = position.coords.latitude;
                        //                         $rootScope.lng = position.coords.longitude;
                        //
                        //                         $rootScope.getDealsWithLocation($rootScope.lat, $rootScope.lng);
                        //
                        //                     }, function (err) {
                        //
                        //                         $rootScope.getDealsWithoutLocation();
                        //
                        //                     });
                        //             }
                        //
                        //                 break;
                        //
                        //             default:
                        //
                        //                 $rootScope.getDealsWithoutLocation();
                        //                 break;
                        //         }
                        //
                        //     });

                    });

            });

        } else {

            var posOptions = {timeout: 3000, enableHighAccuracy: true};

            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                .then(function (position) {

                    $rootScope.lat = position.coords.latitude;
                    $rootScope.lng = position.coords.longitude;

                    $rootScope.getDealsWithLocation($rootScope.lat, $rootScope.lng);

                }, function(err) {

                    $rootScope.getDealsWithoutLocation();
                    console.log('err1', err);

                });

        }

  // on click on enter button
/*
        $rootScope.enterApp = function(){
            iconSpinnerLoad=false;
            document.getElementsByClassName('enterScreen')[0].style="opacity:0;";
            rootScopeGlobal=$rootScope.enterScreen;
            //enterScreenGlobal=$rootScope.enterScreen;
            setTimeout(function(){ 
               rootScopeGlobal= false;
                //document.getElementsByClassName('enterScreen')[0].style="opacity:0;display: none;";

            }, 600);

           
            

        };
*/

        // on click on enter button

        $rootScope.enterApp = function(){

            iconSpinnerLoad=false;
            $rootScope.enterScreen = false;

        };


        // Get data about deals and suppliers with location

        $rootScope.getDealsWithLocation = function (x, y) {

            $rootScope.Deals = [];
            $rootScope.Ads = [];

            var coordinates = {
                'lat': x,
                'lng': y
            };

            $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

            $http.post($rootScope.Host + '/getSapakim.php', coordinates).then(
                function (data) {       //on success
                    console.log('Sapakim: ', data);

                    for (var i = 0; i < data.data.length; i++) {

                        if (data.data[i].deals.length > 0) {

                            for (var j = 0; j < data.data[i].deals.length; j++) {

                                var deal = {
                                    supplierId: data.data[i].index,
                                    dealId: data.data[i].deals[j].index,
                                    address: data.data[i].address,
                                    name: data.data[i].name,
                                    desc: data.data[i].deals[j].desc,
                                    logo: data.data[i].image,
                                    image: data.data[i].deals[j].image2,
                                    starttime: data.data[i].deals[j].startime,
                                    endtime: data.data[i].deals[j].endtime,
                                    deal_date: data.data[i].deals[j].deal_date,
                                    discount_percent: data.data[i].deals[j].discount_precent,
                                    email: data.data[i].email,
                                    phone: data.data[i].phone,
                                    hoursopen: data.data[i].hoursopen,
                                    location_lat: data.data[i].location_lat,
                                    location_lng: data.data[i].location_lng,
                                    dist: data.data[i].deals[j].dist,
                                    distance: 0,
                                    units: "",
                                    quantity_ordered: Number(data.data[i].deals[j].quantity_ordered) + data.data[i].deals[j].quantity_ordered_plus,
                                    kosher: data.data[i].kosher,
                                    msg: 'Hello, ' + data.data[i].name + ' has great deal today!',
                                    active: false
                                };

                                

                                if (deal.image == "") {
                                    deal.image = data.data[i].image2;
                                }

                                if (deal.endtime > '05:00' && $rootScope.timeNow > '05:00') {

                                    if ($rootScope.timeNow <= deal.endtime){

                                        deal.active = true;

                                    }

                                } else if (deal.endtime <= '05:00' && $rootScope.timeNow > '05:00') {

                                    deal.active = true;

                                } else if (deal.endtime <= '05:00' && $rootScope.timeNow <= '05:00') {

                                    if ($rootScope.timeNow <= deal.endtime){

                                        deal.active = true;

                                    }

                                }

                                if (deal.dist >= 1){

                                    deal.distance = deal.dist;
                                    deal.units = "ק״מ" ;

                                } else {

                                    deal.distance = deal.dist * 1000;
                                    deal.units = "מ" ;

                                }
                                if(deal.desc.indexOf('פרסומת:')!==-1){
                                    $rootScope.Ads.push(deal);
                                }
                                else{
                                    $rootScope.Deals.push(deal);
                                    $rootScope.images.push($rootScope.Host + deal.image);
                                    $rootScope.images.push($rootScope.Host + deal.logo);
                                }
                                

                            }
                        }

                    }

                    console.log("DealsWithLocation", $rootScope.Deals);
                },
                function () {           // on error
                    $ionicPopup.alert({
                        title: 'אין התחברות לבסיס נתונים 144',
                        buttons: [{
                            text: 'אשר',
                            type: 'button-positive'
                        }]
                    });
                }
            ).then(function () {
                $ImageCacheFactory.Cache(
                    $rootScope.images
                ).then(function () {

                    $ionicLoading.hide();

                });
            });

        };

        // Get data about deals and suppliers without location

        $rootScope.getDealsWithoutLocation = function () {

            $rootScope.Deals = [];

            $http.get($rootScope.Host + '/getSapakimWithoutDistance.php').then(
                function (data) {       //on success
                    console.log('SapakimWithoutDistance: ', data);

                    // Get information about deals

                    for (var i = 0; i < data.data.length; i++) {

                        if (data.data[i].deals.length > 0) {

                            for (var j = 0; j < data.data[i].deals.length; j++) {

                                var deal = {
                                    supplierId: data.data[i].index,
                                    dealId: data.data[i].deals[j].index,
                                    address: data.data[i].address,
                                    name: data.data[i].name,
                                    logo: data.data[i].image,
                                    image: data.data[i].deals[j].image2,
                                    starttime: data.data[i].deals[j].startime,
                                    endtime: data.data[i].deals[j].endtime,
                                    deal_date: data.data[i].deals[j].deal_date,
                                    discount_percent: data.data[i].deals[j].discount_precent,
                                    email: data.data[i].email,
                                    phone: data.data[i].phone,
                                    hoursopen: data.data[i].hoursopen,
                                    location_lat: data.data[i].location_lat,
                                    location_lng: data.data[i].location_lng,
                                    dist: data.data[i].deals[j].dist,
                                    quantity_ordered: Number(data.data[i].deals[j].quantity_ordered) + data.data[i].deals[j].quantity_ordered_plus,
                                    kosher: data.data[i].kosher,
                                    msg: 'Hello, ' + data.data[i].name + ' has great deal today!',
                                    active: false
                                };

                                  

                                if (deal.image == "") {
                                    deal.image = data.data[i].image2;
                                }

                                if (deal.endtime > '05:00' && $rootScope.timeNow > '05:00') {

                                    if ($rootScope.timeNow <= deal.endtime){

                                        deal.active = true;

                                    }

                                } else if (deal.endtime <= '05:00' && $rootScope.timeNow > '05:00') {

                                    deal.active = true;

                                } else if (deal.endtime <= '05:00' && $rootScope.timeNow <= '05:00') {

                                    if ($rootScope.timeNow <= deal.endtime){

                                        deal.active = true;

                                    }

                                }

                                $rootScope.Deals.push(deal);
                                $rootScope.images.push($rootScope.Host + deal.image);
                                $rootScope.images.push($rootScope.Host + deal.logo);

                            }
                        }

                    }

                    console.log("DealsWithoutLocation", $rootScope.Deals);

                },
                function () {           // on error
                    $ionicPopup.alert({
                        title: 'אין התחברות לבסיס נתונים 1',
                        buttons: [{
                            text: 'אשר',
                            type: 'button-positive'
                        }]
                    });
                }
            ).then(function () {
                $ImageCacheFactory.Cache(
                    $rootScope.images
                ).then(function () {

                    $ionicLoading.hide();

                });
            })
        };


        // get data about Favorites

        var send_data = {
            'user': $localStorage.clientid
        };

        $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

        $http.post($rootScope.Host + '/getFavorites.php', send_data).then(
            function (data) {       // on success

                for (var i = 0; i < data.data.length; i++) {

                    var x = data.data[i].supplier_id;

                    $rootScope.favoriteSuppliers[x] = data.data[i].supplier;

                }

                console.log('favoriteSuppliers', $rootScope.favoriteSuppliers);

            },

            function () {           // on error

                $ionicPopup.alert({
                    title: 'אין התחברות לבסיס נתונים 2',
                    buttons: [{
                        text: 'אשר',
                        type: 'button-positive'
                    }]
                });

            });

        // get data about Orders

        $http.post($rootScope.Host + '/getOrders.php', send_data).then(
            function (data) {       // on success

                // console.log('getOrders', data);

                for (var i = 0; i < data.data.length; i++) {

                    if (data.data[i].deals.length > 0) {

                        for (var j = 0; j < data.data[i].deals.length; j++) {

                            var deal = {
                                supplierId: data.data[i].supplierid,
                                dealId: data.data[i].deals[j].index,
                                address: data.data[i].supplier.address,
                                name: data.data[i].supplier.name,
                                logo: data.data[i].supplier.image,
                                image: data.data[i].deals[j].image2,
                                starttime: data.data[i].deals[j].startime,
                                endtime: data.data[i].deals[j].endtime,
                                deal_date: data.data[i].deals[j].deal_date,
                                discount_percent: data.data[i].deals[j].discount_precent,
                                email: data.data[i].supplier.email,
                                phone: data.data[i].supplier.phone,
                                hoursopen: data.data[i].supplier.hoursopen,
                                quantity_ordered: Number(data.data[i].deals[j].quantity_ordered) + data.data[i].deals[j].quantity_ordered_plus

                            };

                            if (deal.image == "") {
                                deal.image = data.data[i].supplier.image2;
                            }

                            $rootScope.myOrders.push(deal);

                        }
                    }

                }

                console.log('myOrders', $rootScope.myOrders);

            },

            function () {           // on error

                $ionicPopup.alert({
                    title: 'אין התחברות לבסיס נתונים 3',
                    buttons: [{
                        text: 'אשר',
                        type: 'button-positive'
                    }]
                });

            });


    })

    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })

            .state('app.main', {
                url: '/main',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/main.html',
                        controller: 'MainCtrl'
                    }
                }
            })

            .state('app.myorders', {
                url: '/myorders',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/my_orders.html',
                        controller: 'MyOrdersCtrl'
                    }
                }
            })

            .state('app.favorites', {
                url: '/favorites',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/favorites.html',
                        controller: 'FavoritesCtrl'
                    }
                }
            })

            .state('app.add_favorites', {
                url: '/add_favorites',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/add_favorites.html',
                        controller: 'FavoritesCtrl'
                    }
                }
            })

            .state('app.supplier_settings', {
                url: '/supplier_settings',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/supplier_settings.html',
                        controller: 'SupplierCtrl'
                    }
                }
            })

            .state('app.login', {
                url: '/login',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/login.html',
                        controller: 'LoginCtrl'
                    }
                }
            })
        .state('app.conditions_business', {
                url: '/conditions_business',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/conditions_business.html'
                    }
                }
            })
            .state('app.conditions', {
                url: '/conditions',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/conditions.html'
                    }
                }
            });
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/main');

    });
